variable resource_group {
 type = string
 default = "morph-testing"
}

variable "location" {
  type    = string
  default = "switzerland north"
}

variable "dnsprefix" {
  type    = string
  description = "DNS prefix must contain between 2 and 45 characters. The name can contain only letters, numbers, and hyphens."
}

variable "clusterName" {
  type = string
  description = "Cluster Name must contain between 2 and 45 characters. The name can contain only letters, numbers, and hyphens."
}

variable "acrName" {
  type = string
  description = "Set Container Registry Name. Name can contain only alphanumeric values."
}