# Deploy AKS Cluster
data "azurerm_resource_group" "example" {
  name = var.resgrp
}

resource "azurerm_container_registry" "example" {
  name                = var.acrName
  resource_group_name = var.resgrp
  location            = var.location
  sku                 = "Standard"
}

resource "azurerm_public_ip" "example" {
  name                = "aksClusterPublicIp"
  resource_group_name = var.resgrp
  location            = var.location
  allocation_method   = "Static"
  sku                 = "Standard"
  domain_name_label   = var.clusterName
  
  tags = {
    source = "Morpheus"
  }
}

resource "azurerm_kubernetes_cluster" "example" {
  name                = var.clusterName
  location            = var.location
  resource_group_name = var.resgrp
  dns_prefix          = var.dnsprefix
  kubernetes_version  = "1.23.12"

  default_node_pool {
    name       = "default"
    node_count = 3 #as variable
    vm_size    = "Standard_D2_v2" #as variable (optionlist incl. Standard_D3_v2)
  }

  identity {
    type = "SystemAssigned"
  }
  
  network_profile {
    network_plugin     = "kubenet"
    load_balancer_sku  = "standard"
    load_balancer_profile {
        outbound_ip_address_ids = [ azurerm_public_ip.example.id ]
    }
  }

  tags = {
    source = "Morpheus"
  }
}

resource "azurerm_role_assignment" "publicIP" {
  principal_id                     = azurerm_kubernetes_cluster.example.identity[0].principal_id
  role_definition_name             = "Network Contributor"
  scope                            = data.azurerm_resource_group.example.id
  skip_service_principal_aad_check = true
}

resource "azurerm_role_assignment" "containerRegistry" {
  principal_id                     = azurerm_kubernetes_cluster.example.kubelet_identity[0].object_id
  role_definition_name             = "AcrPull"
  scope                            = azurerm_container_registry.example.id
  skip_service_principal_aad_check = true
}

# apply ingress controller on aks
data "kubectl_path_documents" "manifests" {
    pattern = "./manifests/ingress.yaml"
    vars = {
        aks_cluster_name = var.clusterName
		resgrp           = var.resgrp
		public_lb_ip     = azurerm_public_ip.example.id
    }
}

resource "kubectl_manifest" "example" {
    for_each  = toset(data.kubectl_path_documents.manifests.documents)
    yaml_body = each.value
}

output "aks_public_dns" {
  value     = azurerm_public_ip.example.fqdn
  sensitive = true
}

output "acr_service_principal" {
  value     = azurerm_kubernetes_cluster.example.kubelet_identity[0]
  sensitive = true
}

output "client_certificate" {
  value     = azurerm_kubernetes_cluster.example.kube_config.0.client_certificate
  sensitive = true
}

output "kube_config" {
  value     = azurerm_kubernetes_cluster.example.kube_config_raw
  sensitive = true
}