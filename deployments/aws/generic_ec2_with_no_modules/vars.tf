variable "region" {
  type = string
  description = "Region in which to deploy the instance"
}

variable "access_key" {
  type =string
  description = "AWS Access Key"
  sensitive = true
}

variable "secret_key" {
  type = string
  description = "AWS Secret Key"
  sensitive = true
}

variable "app_name" {
  type = string
  description = "Name for the instance"
}

variable "instance_size" {
  type = string
  description = "Must select from one of the following: t2.micro,t2.medium,m4.large,m4.xlarge"

#   validation {
#     condition = contains(
#         ["t2.micro","t2.medium","m4.large","m4.xlarge"], var.instance_size
#     )
#     error_message = "Must select from one of the following: t2.micro,t2.medium,m4.large,m4.xlarge"
#   }
}

variable "instance_count" {
  type = number
}
