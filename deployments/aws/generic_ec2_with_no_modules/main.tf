data "aws_ami" "ubuntu_2004_latest" {
    most_recent = true
    owners = ["099720109477"] # Canonical
    filter {
        name = "name"
        values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
    }
    filter {
        name = "virtualization-type"
        values = ["hvm"]
    }   
}

locals {
    size = "${var.instance_size == "m4.xlarge" ? "m4.xlarge" : ( var.instance_size == "m4.large" ? "m4.large" : ( var.instance_size == "t2.medium" ? "t2.medium" : "t2.micro" )) }"
    ami_id = data.aws_ami.ubuntu_2004_latest.id
}

resource "aws_vpc" "my_vpc" {
  cidr_block = "172.16.0.0/16"

  tags = {
    Name = var.app_name
  }
}

resource "aws_subnet" "my_subnet" {
  vpc_id            = aws_vpc.my_vpc.id
  cidr_block        = "172.16.10.0/24"
  availability_zone = "${var.region}a"

  tags = {
    Name = var.app_name
  }
}

resource "aws_network_interface" "foo" {
  count = var.instance_count
  subnet_id   = aws_subnet.my_subnet.id
  private_ips = ["172.16.10.10${count.index}"]

  tags = {
    Name = "${var.app_name}_if_${count.index}"
  }
}

resource "aws_instance" "foo" {
  count = var.instance_count
  ami           = local.ami_id
  instance_type = var.instance_size

  network_interface {
    network_interface_id = aws_network_interface.foo[count.index].id
    device_index         = 0
  }

  tags = {
    Name = "${var.app_name}_i_${count.index}"
  }

}