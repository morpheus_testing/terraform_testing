variable "region" {
  type = string
  description = "Region in which to deploy the instance"
}

variable "access_key" {
  type =string
  description = "AWS Access Key"
  sensitive = true
}

variable "secret_key" {
  type = string
  description = "AWS Secret Key"
  sensitive = true
}

variable "instance_name" {
  type = string
  description = "Name for the instance"
}

variable "instance_size" {
  type = string
  description = "Must select from one of the following: t2.micro,t2.medium,m4.large,m4.xlarge"

  # validation {
  #   condition = contains(
  #       ["small","medium","large","xl"], var.instance_type
  #   )
  #   error_message = "Must select from one of the following: small,medium,large,xl"
  # }
}

variable "ssh_key_name" {
  type = string
  description = "Name of the SSH key pair (already in AWS) which you would like to add to this instance"
  
}

variable "security_group_ids" {
  type = list(string)
  description = "ID of the security group you wish to deploy into"
}

variable "subnet_id" {
  type =string
  description = "ID of the subnet you wish to deploy into"
}

variable "ssh_user" {
  type = string
  description = "SSH user to create"
}

variable "ssh_password" {
  type = string
  sensitive = true
  description = "Password for the SSH user to create"
}

variable "install_morpheus_agent" {
  type = bool
  description = "Install the Morpheus Agent? (true or false). True only works when running from Morpheus."
}
