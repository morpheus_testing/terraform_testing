terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = ">= 3.35.0"
    }
  }
}

provider "aws" {
    region     = var.region
    access_key = var.access_key
    secret_key = var.secret_key
}

variable "access_key" {
  type = string
  sensitive = true
}

variable "secret_key" {
  type = string
  sensitive = true
}

variable "region" {
  type = string
}

variable "vpc_cidr_block" {
  type = string
}

variable "landing_zone_name" {
  type = string
}

module "vpc" {
  source = "../../../modules/aws/vpc"

  vpc_name = var.landing_zone_name
  vpc_cidr_block = var.vpc_cidr_block

}

output "vpc" {
  value = module.vpc
}

module "subnet" {
  source = "../../../modules/aws/subnet"

  vpc_id = module.vpc.vpc.id
  subnet_name = var.landing_zone_name
  subnet_cidr_block = var.vpc_cidr_block
}