terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = ">= 3.35.0"
    }
  }
}

provider "aws" {
    region     = var.region
    access_key = var.access_key
    secret_key = var.secret_key
}

variable "region" {
  type = string
  description = "Region in which to deploy the instance"
}

variable "access_key" {
  type =string
  description = "AWS Access Key"
  sensitive = true
}

variable "secret_key" {
  type = string
  description = "AWS Secret Key"
  sensitive = true
}