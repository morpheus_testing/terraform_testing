variable "vpc_cidr_block" {
  type = string
}

variable "landing_zone_name" {
  type = string
}