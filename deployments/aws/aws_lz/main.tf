module "aws_lz" {
  source = "git::https://gitlab.com/morpheus_testing/terraform_testing.git//modules/aws/aws_lz_with_sub_modules"

  vpc_cidr_block = var.vpc_cidr_block
  landing_zone_name = var.landing_zone_name
}