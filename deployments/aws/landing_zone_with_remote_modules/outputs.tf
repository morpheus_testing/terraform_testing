output "vpc" {
  value = aws_vpc.main
}

output "igw" {
  value = aws_internet_gateway.main
}

output "route_table" {
  value = aws_route_table.main
}

output "route" {
  value = aws_route.main 
}

output "route_table_association" {
  value = aws_route_table_association.web_subnets
}

output "web_subnet" {
  value = aws_subnet.web_subnets
}