# Landing Zone with Remote Modules

This deployment creates a VPC and subnet specifically referencing the public aws vpc module in the Terraform registry

**Deployment Types:**
- TF Application Blueprint
- TF Instance Type from directory
- TF Instance Type from file templates

**You will need top provide the following vars:**
- region
- access_key
- secret_key
- vpc_cidr_block
- landing_zone_name