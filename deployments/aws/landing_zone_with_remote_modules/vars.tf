variable "account_name" {
  type = string
}

variable "account_email" {
  type = string
}

variable "organizational_unit_id" {
  type = string
}

variable "vpc_cidr_block" {
  type = string
}

variable "landing_zone_name" {
  type = string
}