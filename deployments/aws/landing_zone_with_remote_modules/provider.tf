terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = ">= 3.35.0"
    }
  }
}

provider "aws" {
    alias = "root_account"
    region = var.region
    access_key = var.access_key
    secret_key = var.secret_key
}

provider "aws" {
    region = var.region
    access_key = var.access_key
    secret_key = var.secret_key

    assume_role {
        # The role ARN within Account B to AssumeRole into.
        role_arn = "arn:aws:iam::${aws_organizations_account.lz_account.id}:role/OrganizationAccountAccessRole"
    }
}

variable "access_key" {}
variable "secret_key" {}
variable "region" {}