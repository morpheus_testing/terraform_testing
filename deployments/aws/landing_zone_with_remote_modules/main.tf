resource "aws_organizations_account" "lz_account" {
  provider = aws.root_account

  name = var.account_name
  email = var.account_email
  
  close_on_deletion = true
    ## You can only close 10% of active member accounts within a rolling 30 day period. 
    ## This quota is not bound by a calendar month, but starts when you close an account. 
    ## Within 30 days of that initial account closure, you can't exceed the 10% account closure limit.
    ## https://docs.aws.amazon.com/organizations/latest/APIReference/API_CloseAccount.html
  parent_id = var.organizational_unit_id

  # lifecycle {
  #   ignore_changes = [var.role_name]
  # }
}

resource "aws_vpc" "main" {
  cidr_block = var.vpc_cidr_block

  tags = {
    "Name" = "${var.landing_zone_name}-vpc"
  }
}

resource "aws_internet_gateway" "main" {
  vpc_id = aws_vpc.main.id

  tags = {
    "Name" = "${var.landing_zone_name}-igw"
  }

  depends_on = [
    aws_vpc.main
  ]
}

resource "aws_route_table" "main" {
  vpc_id = aws_vpc.main.id

  depends_on = [
    aws_vpc.main,
    aws_internet_gateway.main
  ]

  tags = {
    "Name" = "${var.landing_zone_name}-rt"
  }
}

resource "aws_route" "main" {
  route_table_id = aws_route_table.main.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id = aws_internet_gateway.main.id

  depends_on = [
    aws_route_table.main,
    aws_internet_gateway.main
  ]
}

resource "aws_route_table_association" "web_subnets" {
  count = length(local.web_cidrs)

  subnet_id = aws_subnet.web_subnets[count.index].id
  route_table_id = aws_route_table.main.id

  depends_on = [
    aws_subnet.web_subnets,
    aws_route_table.main,
    aws_internet_gateway.main
  ]

}

resource "aws_subnet" "web_subnets" {
  count = length(local.web_cidrs)

  vpc_id = aws_vpc.main.id
  availability_zone = local.azs[count.index]
  cidr_block = local.web_cidrs[count.index]
  map_public_ip_on_launch = true

  tags = {
    "Name" = "${var.landing_zone_name}-${local.azs[count.index]}-net"
  }

  depends_on = [
    aws_vpc.main,
    aws_internet_gateway.main
  ]
}

resource "aws_route_table_association" "service_subnets" {
  count = length(local.service_cidrs)

  subnet_id = aws_subnet.service_subnets[count.index].id
  route_table_id = aws_route_table.main.id

  depends_on = [
    aws_subnet.service_subnets,
    aws_route_table.main,
    aws_internet_gateway.main
  ]

}

resource "aws_subnet" "service_subnets" {
  count = length(local.service_cidrs)

  vpc_id = aws_vpc.main.id
  availability_zone = local.azs[count.index]
  cidr_block = local.service_cidrs[count.index]
  map_public_ip_on_launch = true

  tags = {
    "Name" = "${var.landing_zone_name}-${local.azs[count.index]}-net"
  }

  depends_on = [
    aws_vpc.main,
    aws_internet_gateway.main
  ]
}

resource "aws_route_table_association" "db_subnets" {
  count = length(local.db_cidrs)

  subnet_id = aws_subnet.db_subnets[count.index].id
  route_table_id = aws_route_table.main.id

  depends_on = [
    aws_subnet.db_subnets,
    aws_route_table.main,
    aws_internet_gateway.main
  ]

}

resource "aws_subnet" "db_subnets" {
  count = length(local.db_cidrs)

  vpc_id = aws_vpc.main.id
  availability_zone = local.azs[count.index]
  cidr_block = local.db_cidrs[count.index]
  map_public_ip_on_launch = true

  tags = {
    "Name" = "${var.landing_zone_name}-${local.azs[count.index]}-net"
  }

  depends_on = [
    aws_vpc.main,
    aws_internet_gateway.main
  ]
}