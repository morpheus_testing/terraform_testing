locals {
  azs = data.aws_availability_zones.available.names
  subnet_cidrs = cidrsubnets(var.vpc_cidr_block,2,2,2)
  web_cidrs = cidrsubnets(local.subnet_cidrs[0],2,2,2)
  service_cidrs = cidrsubnets(local.subnet_cidrs[1],2,2,2)
  db_cidrs = cidrsubnets(local.subnet_cidrs[2],2,2,2)
}
