data "aws_ami" "ubuntu_2004_latest" {
    most_recent = true
    owners = ["099720109477"] # Canonical
    filter {
        name = "name"
        values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
    }
    filter {
        name = "virtualization-type"
        values = ["hvm"]
    }   
}

locals {
    size = "${var.instance_size == "m4.xlarge" ? "m4.xlarge" : ( var.instance_size == "m4.large" ? "m4.large" : ( var.instance_size == "t2.medium" ? "t2.medium" : "t2.micro" )) }"
    ami_id = data.aws_ami.ubuntu_2004_latest.id
}

module "ec2-instance" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "4.1.4"

  name = var.instance_name
  
  ami = local.ami_id
  instance_type = local.size

  key_name = var.ssh_key_name
  vpc_security_group_ids = var.security_group_ids
  subnet_id = var.subnet_id

  associate_public_ip_address = true
  
  user_data = <<-EOF
#cloud-config
users:
  - name: ${ssh_user}
    sudo: ['ALL=(ALL) NOPASSWD:ALL']
    groups: [sudo]
    shell: /bin/bash
    lock_passwd: false
ssh_pwauth: True
chpasswd:
  list: |
    ${ssh_user}:${ssh_password}
  expire: False
runcmd:
- apt update -y
- <%=instance.cloudConfig.agentInstall%>
- <%=instance.cloudConfig.finalizeServer%>
EOF

}