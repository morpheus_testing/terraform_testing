data "aws_ami" "ubuntu_2004_latest" {
    most_recent = true
    owners = ["099720109477"] # Canonical
    filter {
        name = "name"
        values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
    }
    filter {
        name = "virtualization-type"
        values = ["hvm"]
    }   
}

locals {
    size = "${var.instance_size == "m4.xlarge" ? "m4.xlarge" : ( var.instance_size == "m4.large" ? "m4.large" : ( var.instance_size == "t2.medium" ? "t2.medium" : "t2.micro" )) }"
    ami_id = data.aws_ami.ubuntu_2004_latest.id
    agent_install_command = "install_command_placeholder"
    finalize_server_command = "finalize_server_placeholder"
    user_data = "${var.install_morpheus_agent == true ? "with_agent.txt" : "no_agent.txt"}"
    user_data_vars =  "${var.install_morpheus_agent == false ? { ssh_user = var.ssh_user, ssh_password = var.ssh_password } : { ssh_user = var.ssh_user, ssh_password = var.ssh_password, agent_install = local.agent_install_command, finalize_server = local.finalize_server_command }}"
}

module "ec2-instance" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "4.1.4"

  name = var.instance_name
  
  ami = local.ami_id
  instance_type = local.size

  key_name = var.ssh_key_name
  vpc_security_group_ids = var.security_group_ids
  subnet_id = var.subnet_id

  associate_public_ip_address = true
  
  user_data = templatefile("${path.module}/templates/${local.user_data}", local.user_data_vars )
}