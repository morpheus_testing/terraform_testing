variable "vpc_cidr_block" {
  type = string
  description = "CIDR block for the new VPC"
}

variable "vpc_name" {
  type = string
  description = "Name for the new VPC"
}