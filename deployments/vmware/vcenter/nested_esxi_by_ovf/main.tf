# Get all them datas
data "vsphere_datacenter" "datacenter" {
  name = var.vsphere_datacenter
}

data "vsphere_compute_cluster" "cluster" {
  name = var.vsphere_compute_cluster
  datacenter_id = data.vsphere_datacenter.datacenter.id
}

data "vsphere_host" "host" {
 name = var.vsphere_host
 datacenter_id = data.vsphere_datacenter.datacenter.id 
}

data "vsphere_datastore" "datastore" {
  name = var.vsphere_datastore
  datacenter_id = data.vsphere_datacenter.datacenter.id
}

data "vsphere_resource_pool" "resource_pool" {
  name = var.vsphere_resource_pool
  datacenter_id = data.vsphere_datacenter.datacenter.id
}

data "vsphere_network" "network" {
  name = var.vsphere_network
  datacenter_id = data.vsphere_datacenter.datacenter.id
}

data "vsphere_folder" "folder" {
  path = var.vsphere_folder
}

data "vsphere_ovf_vm_template" "ovf" {
  name = "foo"
  disk_provisioning = "thin"
  resource_pool_id = data.vsphere_resource_pool.resource_pool.id
  datastore_id = data.vsphere_datastore.datastore.id
  remote_ovf_url = var.ovf_url
  ovf_network_map = {
    "labs-seg-01" = data.vsphere_network.network.id
  }
  host_system_id = data.vsphere_host.host.id
}


# Deploy the thing
resource "vsphere_virtual_machine" "deployed_ovf" {
  name = var.vm_name
  folder = trimprefix(data.vsphere_folder.folder.path,"/${data.vsphere_datacenter.datacenter.name}/vm")
  datacenter_id = data.vsphere_datacenter.datacenter.id
  datastore_id = data.vsphere_datastore.datastore.id
  host_system_id = data.vsphere_host.host.id
  resource_pool_id = data.vsphere_resource_pool.resource_pool.id

  # OVF Static vars
  num_cpus = data.vsphere_ovf_vm_template.ovf.num_cpus
  num_cores_per_socket = data.vsphere_ovf_vm_template.ovf.num_cores_per_socket
  memory = data.vsphere_ovf_vm_template.ovf.memory
  guest_id = data.vsphere_ovf_vm_template.ovf.guest_id
  scsi_type = data.vsphere_ovf_vm_template.ovf.scsi_type
  nested_hv_enabled = data.vsphere_ovf_vm_template.ovf.nested_hv_enabled
  dynamic "network_interface" {
    for_each = data.vsphere_ovf_vm_template.ovf.ovf_network_map
        content {
            network_id = network_interface.value
        }
  }
  wait_for_guest_ip_timeout = 0
  wait_for_guest_net_timeout = 0

  ovf_deploy {
    allow_unverified_ssl_cert = true
    remote_ovf_url = data.vsphere_ovf_vm_template.ovf.remote_ovf_url
    disk_provisioning = data.vsphere_ovf_vm_template.ovf.disk_provisioning
    ovf_network_map = data.vsphere_ovf_vm_template.ovf.ovf_network_map
  }

  vapp {
    properties = {
      "guestinfo.hostname" = var.vm_name,
      "guestinfo.ipaddress" = var.vm_ip_address,
      "guestinfo.netmask" = var.vm_netmask,
      "guestinfo.gateway" = var.vm_gateway,
      "guestinfo.dns" = var.vm_dns_server,
      "guestinfo.domain" = var.vm_domain,
      "guestinfo.ntp" = var.vm_ntp_server,
      "guestinfo.password" = var.root_password,
      "guestinfo.ssh" = "True"
    }
  }

  lifecycle {
    ignore_changes = [
      annotation,
      disk[0].io_share_count,
      disk[1].io_share_count,
      disk[2].io_share_count,
      vapp[0].properties,
    ]
  }
}
