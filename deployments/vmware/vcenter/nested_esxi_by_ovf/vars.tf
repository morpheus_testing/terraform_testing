variable "vsphere_datacenter" {
  type = string
}

variable "vsphere_datastore" {
  type = string
}

variable "vsphere_compute_cluster" {
  type = string
}

variable "vsphere_resource_pool" {
  type = string
}

variable "vsphere_host" {
  type = string
}

variable "vsphere_network" {
  type = string
}

variable "vsphere_folder" {
  type = string
}

variable "ovf_url" {
  type = string
  description = "The location of the OVF template. URL or Local"
}


# Guest Info
variable "vm_name" {
  type = string
}

variable "vm_ip_address" {
  type = string
}

variable "vm_netmask" {
  type = string
}

variable "vm_gateway" {
  type = string
}

variable "vm_dns_server" {
  type = string
}

variable "vm_domain" {
  type = string
}

variable "vm_ntp_server" {
  type = string
}

variable "root_password" {
  type = string
}