terraform {
  required_providers {
    random = {
      source = "hashicorp/random"
      ## Keep one version commented
      
      ## Starting Version
      # version = "3.4.0"
      ## Upgrading version
      version = "3.4.3"
    }
  }
}

provider "random" {
  # Configuration options
}