resource "random_integer" "number" {
  min = 1
  max = 250
}

resource "random_string" "string" {
  length = 8
  special = false
}

resource "random_shuffle" "bool" {
  input = ["true","false"]
  result_count = 1
}

locals {
  bool = random_shuffle.bool.result == "true" ? true : false
}

module "primitive_vars" {
  source = "./primitive_vars"

  string = random_string.string.result
  number = random_integer.number.result
  bool = local.bool
}