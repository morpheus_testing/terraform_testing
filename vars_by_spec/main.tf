module "primitive_vars" {
  source = "git::https://gitlab.com/morpheus_testing/terraform_testing.git//modules/primitive_vars"

  string = var.string
  number = var.number
  bool = true
}

variable "string" {
  type = string
  description = "Please enter a string value"
}

variable "number" {
  type = number
  description = "Please enter a numeric value"
}