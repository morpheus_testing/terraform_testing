 terraform {
  required_providers {
    random = {
      source = "hashicorp/random"
      version = "3.1.0"
    }
  }
}

provider "random" {}

##################################
### PRIMITIVE VARS
##################################

variable "string" {
  type = string
  default = "This is a string"
}

output "string" {
  value = var.string
}

variable "number" {
  type = number
}

output "number" {
  value = var.number
}

variable "bool" {
  type = bool
}

output "bool" {
  value = var.bool
}

#####################################
### SINGLE LAYER COMPLEX VARS
#####################################

variable "list_of_strings" {
  type = list(string)
}

output "list_of_strings" {
  value = var.list_of_strings
}

variable "map_of_strings" {
  type = map(string)
}

output "map_of_strings" {
  value = var.map_of_strings
}

variable "map_of_numbers" {
  type = map(number)
}

output "map_of_numbers" {
  value = var.map_of_numbers
}

variable "map_of_bool" {
  type = map(bool)
}

output "map_of_bool" {
  value = var.map_of_bool
}

#####################################
### MULTI-LAYER COMPLEX VARS
#####################################

variable "map_of_list_of_strings" {
  type = map(list(string))
}

output "map_of_list_of_string" {
    value = var.map_of_list_of_strings 
}

variable "list_of_maps" {
  type = list(map(string))
}

variable "object_3_layer" {
  type = object({
    user_1 = object({
      name = string
      email = string
      type = string
      roles = map(bool)
      team_members = list(string)
    }),
    user_2 = object({
      name = string
      email = string
      type = string
      roles = map(bool)
      team_members = list(string)
    })
  })
}

output "object_3_layer" {
  value = var.object_3_layer
}

variable "object_5_layer" {
  type = object({
    summer_olymipic_games = object({
      year = object({
        year = string
        host_city = string
        host_country = string
        sports = list(string)
        events = object({
          name = string
          medalists = object({
            gold = string
            silver = string
            bronze = string
          })
        })
      })
    }),
    winter_olymipic_games = object({
      year = object({
        year = string
        host_city = string
        host_country = string
        sports = list(string)
        events = object({
          name = string
          medalists = object({
            gold = string
            silver = string
            bronze = string
          })
        })
      })
    })
  })
}

output "object_5_layer" {
  value = var.object_5_layer
}

output "contact_types" {
  value = [ for contact in var.list_of_maps : contact.type]
}

output "contact_names" {
  value = [ for contact in var.list_of_maps : contact.name]
}

output "contact_emails" {
  value = [ for contact in var.list_of_maps : contact.email]
}

output "contact_phones" {
  value = [ for contact in var.list_of_maps : contact.phone]
}