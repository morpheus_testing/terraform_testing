 terraform {
  required_providers {
    random = {
      source = "hashicorp/random"
      version = "3.1.0"
    }
  }
}

provider "random" {
}

##################################
### PRIMITIVE VARS
##################################

variable "string" {
  type = string
  default = "This is a string"
}

output "string" {
  value = var.string
}

variable "number" {
  type = number
  default = 42
}

output "number" {
  value = var.number
}

variable "bool" {
  type = bool
  default = true
}

output "bool" {
  value = var.bool
}

variable "sensitive_value" {
  type = string
  sensitive = true
}

output "sensitive_value" {
  value = var.sensitive_value
  sensitive = true
}

variable "tfvar_test" {
  type = string
}

output "tfvar_test" {
  value = var.tfvar_test
}

#####################################
### SINGLE LAYER COMPLEX VARS
#####################################

variable "list_of_strings" {
  type = list(string)
  default = [ "string_1","string_2","string_3" ]
}

output "list_of_strings" {
  value = var.list_of_strings
}

variable "map_of_strings" {
  type = map(string)
 default = {
   "string_1" = "This is string 1"
   "string_2" = "This is string 2"
 }
}

output "map_of_strings" {
  value = var.map_of_strings
}

variable "map_of_numbers" {
  type = map(number)

  default = {
    "number_1" = 1
    "number_2" = 2
  }
}

output "map_of_numbers" {
  value = var.map_of_numbers
}

variable "map_of_bool" {
  type = map(bool)

  default = {
    "True" = true
    "False" = false
  }
}

output "map_of_bool" {
  value = var.map_of_bool
}

#####################################
### MULTI-LAYER COMPLEX VARS
#####################################

variable "map_of_list_of_strings" {
  type = map(list(string))

  default = {
    "list1" : [ "string1-1","string1-2","string1-3" ]
    "list2" : [ "string2-1","string2-2","string2-3"]
  }
}

output "map_of_list_of_string" {
    value = var.map_of_list_of_strings 
}

variable "list_of_maps" {
  type = list(map(string))
  default = [
    {
      "type": "OPERATIONS",
      "name": "Joe Schmo",
      "email": "joe@no.mail",
      "phone": "888-888-8888" 
    },
    {
      "type": "FINANCE",
      "name": "Jane Schmo",
      "email": "jane@no.mail",
      "phone": "888-888-8888" 
    },
    {
      "type": "SECURITY",
      "name": "Butch Catchem",
      "email": "butch@no.mail",
      "phone": "888-888-8888" 
    }
  ]
}

variable "map_of_maps" {
  type = any
  default = {
    "user_1" : {
      "type": "OPERATIONS",
      "name": "Joe Schmo",
      "email": "joe@schmo.biz",
      "roles": {
        "admin": true,
        "user": true,
        "auditor": true
      },
      "team_members": [ "tom", "dick", "harry" ]
    },
    "user_2" : {
      "type": "FINANCE",
      "name": "Jane Schmo",
      "email": "jane@schmo.biz",
      "roles": {
        "admin": true,
        "user": true,
        "auditor": true
      },
      "team_members": [ "peggy", "sue", "bobby" ]
    }
  }
}

output "contact_types" {
  value = [ for contact in var.list_of_maps : contact.type]
}

output "contact_names" {
  value = [ for contact in var.list_of_maps : contact.name]
}

output "contact_emails" {
  value = [ for contact in var.list_of_maps : contact.email]
}

output "contact_phones" {
  value = [ for contact in var.list_of_maps : contact.phone]
}
