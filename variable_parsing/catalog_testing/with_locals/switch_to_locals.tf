 terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = ">= 3.35.0"
    }
  }
}

provider "aws" {
}

##################################
### PRIMITIVE VARS
##################################

variable "string" {
  type = string
  default = "This is a string"
}

output "string" {
  value = var.string
}

variable "number" {
  type = number
  default = 42
}

output "number" {
  value = var.number
}

variable "bool" {
  type = bool
  default = true
}

output "bool" {
  value = var.bool
}


locals {
  list_of_strings = "<%=cusotmOptions.listString%>"
  map_of_strings = "<%=customOptions.mapString%>"
  list_of_maps_of_strings = "<%=customOptions.listMapString%>"
  map_of_list_of_string = "<%=customOptions.mapListString%>"
  map_object = jsondecode(map_of_list_of_string)
  list_object = jsondecode(list_of_maps_of_strings)
}

#####################################
### SINGLE LAYER COMPLEX VARS
#####################################

# variable "list_of_strings" {
#   type = list(string)
#   # default = [ "string_1","string_2","string_3" ]
# }

output "list_of_strings" {
  # value = var.list_of_strings
  value = local.list_of_strings
}

# variable "map_of_strings" {
#   type = map(string)
#  default = {
#    "string_1" = "This is string 1"
#    "string_2" = "This is string 2"
#  }
# }

output "map_of_strings" {
  # value = var.map_of_strings
  value = local.map_of_strings
}

# variable "map_of_numbers" {
#   type = map(number)

#   default = {
#     "number_1" = 1
#     "number_2" = 2
#   }
# }

# output "map_of_numbers" {
#   value = var.map_of_numbers
# }

# variable "map_of_bool" {
#   type = map(bool)

#   default = {
#     "True" = true
#     "False" = false
#   }
# }

# output "map_of_bool" {
#   value = var.map_of_bool
# }

#####################################
### MULTI-LAYER COMPLEX VARS
#####################################

# variable "map_of_list_of_strings" {
#   type = map(list(string))

#   default = {
#     "list1" : [ "string1-1","string1-2","string1-3" ]
#     "list2" : [ "string2-1","string2-2","string2-3"]
#   }
# }

output "map_of_list_of_string" {
  # value = var.map_of_list_of_strings 
  value = local.map_of_list_of_string
}

# variable "list_of_maps" {
#   type = list(map(string))
#   default = [
  #   {
  #     "type": "OPERATIONS",
  #     "name": "Joe Schmo",
  #     "email": "joe@no.mail",
  #     "phone": "888-888-8888" 
  #   },
  #   {
  #     "type": "FINANCE",
  #     "name": "Jane Schmo",
  #     "email": "jane@no.mail",
  #     "phone": "888-888-8888" 
  #   },
  #   {
  #     "type": "SECURITY",
  #     "name": "Butch Catchem",
  #     "email": "butch@no.mail",
  #     "phone": "888-888-8888" 
  #   }
  # ]
# }

output "contact_types" {
  # value = [ for contact in var.list_of_maps : contact.type]
  value = [ for contact in local.map_object : contact.type]
}

output "contact_names" {
  # value = [ for contact in var.list_of_maps : contact.name]
  value = [ for contact in local.map_object : contact.name]
}

output "contact_emails" {
  # value = [ for contact in var.list_of_maps : contact.email]
  value = [ for contact in local.map_object : contact.email]
}

output "contact_phones" {
  # value = [ for contact in var.list_of_maps : contact.phone]
  value = [ for contact in local.map_object : contact.phone]
}