terraform {
  required_providers {
    random = {
      source = "hashicorp/random"
      version = "3.4.3"
    }
  }
}

provider "random" {
  # Configuration options
}

variable "text_box" {
  type = string
}

output "text_box" {
  value = var.text_box
}

variable "check_box" {
  type = bool
}

output "check_box" {
  value = var.check_box
}

variable "number" {
  type =number
}

output "number" {
  value = var.number
}

variable "radio_list" {
  type = string
}

output "radio_list" {
  value = var.radio_list
}

variable "select_list" {
  type = string
}

output "select_list" {
  value = var.select_list
}

variable "typeahead_single_select" {
  type = string
}

output "typeahead_single_select" {
  value = var.typeahead_single_select
}

variable "typeahead_multi_select" {
  type = list(string)
}

output "typeahead_multi_select" {
  value = var.typeahead_multi_select
}


