resource "null_resource" "string" {
    triggers = {
        string = var.string
    }
}

resource "null_resource" "number" {
    triggers = {
        number = var.number
    }
}

resource "null_resource" "bool" {
    triggers = {
        bool = var.bool
    }
}