output "string" {
  value = null_resource.string.triggers["string"]
}

output "number" {
  value = null_resource.number.triggers["number"]
}

output "bool" {
  value = null_resource.bool.triggers["bool"]
}