##################################
### PRIMITIVE VARS
##################################

variable "string" {
  type = string
}

variable "number" {
  type = number
}

variable "bool" {
  type = bool
}