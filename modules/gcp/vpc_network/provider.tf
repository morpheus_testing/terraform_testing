terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "4.33.0"
    }
  }
}

provider "google" {
  # Configuration options
  project = var.project
  credentials = var.gcp_auth
  # credentials = file("creds.json")

}