variable "vpc_cidr_block" {
  type = string
}

variable "landing_zone_name" {
  type = string
}

module "vpc" {
  source = "./vpc"

  vpc_name = var.landing_zone_name
  vpc_cidr_block = var.vpc_cidr_block

}

output "vpc" {
  value = module.vpc
}

module "subnet" {
  source = "./subnet"

  vpc_id = module.vpc.vpc.id
  subnet_name = var.landing_zone_name
  subnet_cidr_block = var.vpc_cidr_block
}