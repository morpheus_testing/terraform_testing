module "primitive_vars" {
  ### GIT LAB
  ##source = "git::https://oauth2:<%=cypher.read('secret/gitlab_tf_token')%>@gitlab.com/resting_wheeze/private_terraform_modules.git//random/primitive_vars"
  
  ### Linked GIT LAB
  source = "git::https://gitlab.com/resting_wheeze/private_terraform_modules.git//random/primitive_vars"

  ### ADO
  ##source = "git::https://<%=cypher.read('secret/ado_tf_token')%>@dev.azure.com/grunstam/private_terraform_modules/_git/tf-basic-module-ado//random/primitive_vars"

  ### Linked ADO
  # source = "git::https://dev.azure.com/grunstam/private_terraform_modules/_git/tf-basic-module-ado//random/primitive_vars"
  
  string = var.string
  number = var.number
  bool = true
}

variable "string" {
  type = string
  description = "Please enter a string value"
}

variable "number" {
  type = number
  description = "Please enter a number"
}